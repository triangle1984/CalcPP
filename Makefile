CCP=g++
flags=-O3
all: CalcPP
CalcPP: main.cpp game.h.gch
	$(CCP) $(flags) main.cpp -o CalcPP
game.h.gch: game.h
	$(CCP) $(flags) game.h
install: CalcPP
	install ./CalcPP /usr/local/bin
uninstall: 
	rm -f /usr/local/bin/CalcPP
clean: 
	rm -f *.gch
