#include <cstdlib>
#include <iostream>

void calc() {
  int x, y, result;
  char sign;
  while (true) {
    std::cout << "First number: ";
    std::cin >> x;
    std::cout << "Second number: ";
    std::cin >> y;
    std::cout << "Sign: ";
    std::cin >> sign;
    if (!std::cin) {
      std::cout << "\nError!\n";
      std::cin.clear();
      std::cin.get();
      continue;
    }
    if (sign == '+') {
      result = x + y;
    } else if (sign == '-') {
      result = x - y;
    } else {
      std::cout << "+ or -\n";
      continue;
    }
    std::cout << "Result: " << result << '\n';
    break;
  }
}

void chance() {
  int percent;
  std::string event;
  std::cin.clear();
  std::cin.get();
  std::cout << "Enter an event: ";
  std::getline(std::cin, event);
  percent = rand() % 100 + 1;
  std::cout << "The probability that " << event << " equal to " << percent
            << "%\n";
}
